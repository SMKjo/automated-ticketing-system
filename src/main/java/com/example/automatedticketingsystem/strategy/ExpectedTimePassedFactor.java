package com.example.automatedticketingsystem.strategy;

import com.example.automatedticketingsystem.enums.DeliveryStatus;
import com.example.automatedticketingsystem.model.DeliveryDetailDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

@Slf4j
@Component
public class ExpectedTimePassedFactor implements Factor {

    @Override
    public Boolean applyFactor(DeliveryDetailDto detail) {
        if (checkExpectedTimePassed(detail)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public FactorName getFactorName() {
        return FactorName.IsExpectedTimePassed;
    }

    private Boolean checkExpectedTimePassed(DeliveryDetailDto detail) {
        log.info("Expected Delivery time " + detail.getExpectedDeliveryTime() + " is after " + LocalTime.now() + " and delivery status " + detail.getDeliveryStatus());
        if (detail.getExpectedDeliveryTime().isAfter(LocalTime.now()) && !detail.getDeliveryStatus().equals(DeliveryStatus.ORDER_DELIVERED)) {
            log.info("FALSE");
            return false;
        } else {
            log.info("TRUE");
            return true;
        }
    }
}
