package com.example.automatedticketingsystem.service;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.entity.Ticket;
import com.example.automatedticketingsystem.enums.Priority;
import com.example.automatedticketingsystem.mapper.TicketMapper;
import com.example.automatedticketingsystem.model.TicketDto;
import com.example.automatedticketingsystem.repository.TicketRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class TicketServiceI implements TicketService {

    private final TicketRepository ticketsRepository;
    private final TicketMapper ticketMapper;

    @Autowired
    public TicketServiceI(TicketRepository ticketsRepository, TicketMapper ticketMapper) {
        this.ticketsRepository = ticketsRepository;
        this.ticketMapper = ticketMapper;
    }


    @Override
    public List<TicketDto> getAllTickets() {
        List<Ticket> ticketList = ticketsRepository.findAllByOrderByPriorityAsc();
        System.out.println("List ticket " + ticketList);
        List<TicketDto> ticketDtos = new ArrayList<>();
        for (Ticket ticket : ticketList) {
            TicketDto ticketDto = ticketMapper.TicketEntityToDtoMapper(ticket);
            ticketDtos.add(ticketDto);
        }
        System.out.println("List Ticket Dto " + ticketDtos);
        return ticketDtos;
    }

    @Override
    public void createTicket(DeliveryDetail deliveryDetail, Priority priority) {
        try {
            Ticket ticket = new Ticket();
            ticket.setDeliveryDetail(deliveryDetail);
            ticket.setPriority(priority);
            ticketsRepository.save(ticket);
            log.info("ticket created successfully");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unable to create ticket :: " + e.getMessage());
        }
    }

    @Override
    public void updateTicket(Ticket ticket, Priority priority) {
        try {
            if (!ticket.getPriority().equals(priority)) {
                ticket.setPriority(priority);
                ticketsRepository.save(ticket);
                log.info("ticket updated successfully");
            } else {
                log.info("ticket have same priority");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public Ticket getAlreadyCreatedTicket(DeliveryDetail detail) {
        try {
            return ticketsRepository.findTicketByDeliveryDetail(detail);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }


}
