package com.example.automatedticketingsystem.repository;

import com.example.automatedticketingsystem.entity.MyUserDetail;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @AfterEach
    void deleteAllData() {
        userRepository.deleteAll();
    }

    @Test
    void checkfindByUsernameExists() {
        String username = "khizar";
        userRepository.save(new MyUserDetail(1, username, "abcd@1234"));
        MyUserDetail myUserDetail1 = userRepository.findByUsername(username);
        assertEquals(myUserDetail1.getUsername(), username);
    }

    @Test
    void checkfindByUsernameDoesNotExist() {
        String username = "khizar";
        userRepository.save(new MyUserDetail(1, username, "abcd@1234"));
        MyUserDetail myUserDetail1 = userRepository.findByUsername(username);
        assertNotEquals(myUserDetail1.getUsername(), "admin");
    }

}