package com.example.automatedticketingsystem.service;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.enums.CustomerType;
import com.example.automatedticketingsystem.enums.DeliveryStatus;
import com.example.automatedticketingsystem.model.DeliveryDetailDto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CheckDetailToGenerateTicketITest {


    private CheckDetailToGenerateTicketI checkDetailToGenerateTicketI;

    private DeliveryDetail prepareDeliveryDetailForTimeOfDeliveryPassed(CustomerType customerType, LocalTime time, DeliveryStatus deliveryStatus) {
        DeliveryDetail deliveryDetail = new DeliveryDetail();
        deliveryDetail.setDeliveryStatus(deliveryStatus);
        deliveryDetail.setCustomerType(customerType);
        deliveryDetail.setExpectedDeliveryTime(time);
        deliveryDetail.setCurrentDistanceFromDestinationInMeters(50);
        deliveryDetail.setTimeToReachDestination(LocalTime.of(00, 10, 10));
        return deliveryDetail;
    }

    private DeliveryDetailDto prepareDeliveryDetailDto(LocalTime meanTimeToCook, LocalTime timeToReach, LocalTime expectedTime) {
        DeliveryDetailDto deliveryDetailDto = new DeliveryDetailDto();
        deliveryDetailDto.setDeliveryStatus(DeliveryStatus.ORDER_RECEIVED);
        deliveryDetailDto.setCustomerType(CustomerType.LOYAL);
        deliveryDetailDto.setExpectedDeliveryTime(expectedTime);
        deliveryDetailDto.setCurrentDistanceFromDestinationInMeters(50);
        deliveryDetailDto.setTimeToReachDestination(timeToReach);
        deliveryDetailDto.setRiderRating(1);
        deliveryDetailDto.setMeanTimeToPrepareFood(meanTimeToCook);
        return deliveryDetailDto;
    }

    @BeforeEach
    void setUp() {
        checkDetailToGenerateTicketI = new CheckDetailToGenerateTicketI();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void checkTimeOfDeliveryNotPassedTest() {
        //given
        DeliveryDetail detail = prepareDeliveryDetailForTimeOfDeliveryPassed(CustomerType.LOYAL, LocalTime.now().plusMinutes(25), DeliveryStatus.ORDER_RECEIVED);
        //when
        Boolean isTimeOfDeliveryPassed = checkDetailToGenerateTicketI.checkTimeOfDeliveryPassed(detail);
        //then
        assertThat(isTimeOfDeliveryPassed).isFalse();

    }

    @Test
    void checkTimeOfDeliveryPassed() {
        //given
        DeliveryDetail detail = prepareDeliveryDetailForTimeOfDeliveryPassed(CustomerType.LOYAL, LocalTime.now().minusMinutes(15), DeliveryStatus.ORDER_RECEIVED);
        //when
        Boolean isTimeOfDeliveryPassed = checkDetailToGenerateTicketI.checkTimeOfDeliveryPassed(detail);
        //then
        assertThat(isTimeOfDeliveryPassed).isTrue();
    }

    @Test
    void checkTimeOfDeliveryPassedAndOrderDelivered() {
        //given
        DeliveryDetail detail = prepareDeliveryDetailForTimeOfDeliveryPassed(CustomerType.LOYAL, LocalTime.now().minusMinutes(15), DeliveryStatus.ORDER_DELIVERED);
        //when
        Boolean isTimeOfDeliveryPassed = checkDetailToGenerateTicketI.checkTimeOfDeliveryPassed(detail);
        //then
        assertThat(isTimeOfDeliveryPassed).isFalse();
    }


    @Test
    void checkEstimatedTimeOfDeliveryGreaterThanExpectedTest() {
        //given
        DeliveryDetailDto detailDto = prepareDeliveryDetailDto(LocalTime.of(00, 30, 00), LocalTime.of(00, 10, 00), LocalTime.of(01, 30, 00));
        //when
        Boolean isEstimationGreaterThanExpected = checkDetailToGenerateTicketI.checkEstimatedTimeOfDelivery(detailDto);
        //then
        assertThat(isEstimationGreaterThanExpected).isTrue();

    }

    @Test
    void checkEstimatedTimeOfDeliveryNotGreaterThanExpectedTest() {
        //given
        DeliveryDetailDto detailDto = prepareDeliveryDetailDto(LocalTime.of(00, 30, 00), LocalTime.of(00, 10, 00), LocalTime.of(01, 50, 00));
        //when
        Boolean isEstimationGreaterThanExpected = checkDetailToGenerateTicketI.checkEstimatedTimeOfDelivery(detailDto);
        //then
        assertThat(isEstimationGreaterThanExpected).isFalse();

    }

    @Test
    void checkCustomerTypeVIPTest() {
        //given
        DeliveryDetail detail = prepareDeliveryDetailForTimeOfDeliveryPassed(CustomerType.VIP, LocalTime.now().plusMinutes(15), DeliveryStatus.ORDER_RECEIVED);
        //when
        Boolean isVIP = checkDetailToGenerateTicketI.checkCustomerType(detail);
        //then
        assertThat(isVIP).isTrue();
    }

    @Test
    void checkCustomerTypeIsNotVIPTest() {
        //given
        DeliveryDetail detail = prepareDeliveryDetailForTimeOfDeliveryPassed(CustomerType.LOYAL, LocalTime.now().plusMinutes(15), DeliveryStatus.ORDER_RECEIVED);
        //when
        Boolean isVIP = checkDetailToGenerateTicketI.checkCustomerType(detail);
        //then
        assertThat(isVIP).isFalse();
    }
}