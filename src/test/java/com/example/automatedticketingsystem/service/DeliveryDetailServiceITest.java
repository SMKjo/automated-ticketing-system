package com.example.automatedticketingsystem.service;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.enums.CustomerType;
import com.example.automatedticketingsystem.enums.DeliveryStatus;
import com.example.automatedticketingsystem.mapper.DeliveryDetailMapper;
import com.example.automatedticketingsystem.model.DeliveryDetailDto;
import com.example.automatedticketingsystem.repository.DeliveryDetailRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DeliveryDetailServiceITest {

    @Mock
    private DeliveryDetailRepository detailRepository;
    private DeliveryDetailServiceI deliveryDetailServiceI;
    private DeliveryDetailMapper deliveryDetailMapper;
    private TicketServiceI ticketServiceI;
    private CheckDetailToGenerateTicketI checkDetailToGenerateTicketI;

    private DeliveryDetail prepareDeliveryDetail() {
        DeliveryDetail deliveryDetail = new DeliveryDetail();
        deliveryDetail.setDeliveryStatus(DeliveryStatus.ORDER_RECEIVED);
        deliveryDetail.setCustomerType(CustomerType.VIP);
        deliveryDetail.setExpectedDeliveryTime(LocalTime.now().plusMinutes(25));
        deliveryDetail.setCurrentDistanceFromDestinationInMeters(50);
        deliveryDetail.setTimeToReachDestination(LocalTime.of(00, 10, 10));
        return deliveryDetail;
    }

    private DeliveryDetailDto prepareDeliveryDetailDto() {
        DeliveryDetailDto deliveryDetailDto = new DeliveryDetailDto();
        deliveryDetailDto.setDeliveryStatus(DeliveryStatus.ORDER_RECEIVED);
        deliveryDetailDto.setCustomerType(CustomerType.VIP);
        deliveryDetailDto.setExpectedDeliveryTime(LocalTime.now().plusMinutes(25));
        deliveryDetailDto.setCurrentDistanceFromDestinationInMeters(50);
        deliveryDetailDto.setTimeToReachDestination(LocalTime.of(00, 10, 10));
        deliveryDetailDto.setRiderRating(1);
        deliveryDetailDto.setMeanTimeToPrepareFood(LocalTime.of(00, 20, 10));
        return deliveryDetailDto;
    }


    @BeforeEach
    void setUp() {
        deliveryDetailServiceI = new DeliveryDetailServiceI(detailRepository, deliveryDetailMapper, ticketServiceI, checkDetailToGenerateTicketI);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addDeliveryDetails() {
    }

    @Test
    void addDeliveryDetailsStrategy() {

    }

    @Test
    void getAllDeliveryDetails() {
        deliveryDetailServiceI.getAllDeliveryDetails();
        verify(detailRepository).findAll();
    }
}