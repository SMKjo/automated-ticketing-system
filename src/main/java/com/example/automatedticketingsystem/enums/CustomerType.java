package com.example.automatedticketingsystem.enums;

public enum CustomerType {
    VIP, LOYAL, NEW;
}
