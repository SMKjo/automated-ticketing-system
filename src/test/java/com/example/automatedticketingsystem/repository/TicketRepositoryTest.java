package com.example.automatedticketingsystem.repository;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.entity.Ticket;
import com.example.automatedticketingsystem.enums.CustomerType;
import com.example.automatedticketingsystem.enums.DeliveryStatus;
import com.example.automatedticketingsystem.enums.Priority;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@DataJpaTest
class TicketRepositoryTest {

    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    DeliveryDetailRepository deliveryDetailRepository;

    @BeforeEach
    void setupData() {
    }

    @Test
    void FindTicketByDeliveryDetailShouldReturnTrue() {
        DeliveryDetail deliveryDetail = new DeliveryDetail();
        deliveryDetail.setDeliveryStatus(DeliveryStatus.ORDER_RECEIVED);
        deliveryDetail.setCustomerType(CustomerType.VIP);
        deliveryDetail.setExpectedDeliveryTime(LocalTime.now().plusMinutes(25));
        deliveryDetail.setCurrentDistanceFromDestinationInMeters(50);
        deliveryDetail.setTimeToReachDestination(LocalTime.of(00, 10, 10));
        deliveryDetailRepository.save(deliveryDetail);
        Ticket actualTicket = new Ticket(1, deliveryDetail, Priority.HIGH);
        ticketRepository.save(actualTicket);
        Ticket expectedTicket = ticketRepository.findTicketByDeliveryDetail(deliveryDetail);
        assertEquals(expectedTicket, actualTicket);
    }

    @Test
    void FindTicketByDeliveryDetailShouldReturnFalse() {
        DeliveryDetail deliveryDetail = new DeliveryDetail();
        deliveryDetail.setDeliveryStatus(DeliveryStatus.ORDER_RECEIVED);
        deliveryDetail.setCustomerType(CustomerType.VIP);
        deliveryDetail.setExpectedDeliveryTime(LocalTime.now().plusMinutes(25));
        deliveryDetail.setCurrentDistanceFromDestinationInMeters(50);
        deliveryDetail.setTimeToReachDestination(LocalTime.of(00, 10, 10));
        deliveryDetailRepository.save(deliveryDetail);
        DeliveryDetail actualDeliveryDetail = new DeliveryDetail();
        actualDeliveryDetail.setDeliveryStatus(DeliveryStatus.ORDER_RECEIVED);
        actualDeliveryDetail.setCustomerType(CustomerType.NEW);
        actualDeliveryDetail.setExpectedDeliveryTime(LocalTime.now().plusMinutes(25));
        actualDeliveryDetail.setCurrentDistanceFromDestinationInMeters(50);
        actualDeliveryDetail.setTimeToReachDestination(LocalTime.of(00, 10, 10));
        deliveryDetailRepository.save(actualDeliveryDetail);

        Ticket actualTicket = new Ticket(1, actualDeliveryDetail, Priority.HIGH);
        ticketRepository.save(actualTicket);
        Ticket expectedTicket = ticketRepository.findTicketByDeliveryDetail(deliveryDetail);
        assertNotEquals(expectedTicket, actualTicket);

    }

    @Test
    void findAllByOrderByPriorityAsc() {

    }
}