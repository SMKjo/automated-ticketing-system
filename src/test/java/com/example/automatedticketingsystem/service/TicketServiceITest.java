package com.example.automatedticketingsystem.service;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.entity.Ticket;
import com.example.automatedticketingsystem.enums.CustomerType;
import com.example.automatedticketingsystem.enums.DeliveryStatus;
import com.example.automatedticketingsystem.enums.Priority;
import com.example.automatedticketingsystem.mapper.TicketMapper;
import com.example.automatedticketingsystem.repository.TicketRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TicketServiceITest {

    @Mock
    private TicketRepository ticketRepository;
    private TicketServiceI ticketServiceI;
    private TicketMapper ticketMapper;

    private Ticket prepareTicket() {
        DeliveryDetail deliveryDetail = new DeliveryDetail();
        deliveryDetail.setDeliveryStatus(DeliveryStatus.ORDER_RECEIVED);
        deliveryDetail.setCustomerType(CustomerType.VIP);
        deliveryDetail.setExpectedDeliveryTime(LocalTime.now().plusMinutes(25));
        deliveryDetail.setCurrentDistanceFromDestinationInMeters(50);
        deliveryDetail.setTimeToReachDestination(LocalTime.of(00, 10, 10));
        Ticket ticket = new Ticket(1, deliveryDetail, Priority.HIGH);
        return ticket;
    }

    private DeliveryDetail prepareDeliveryDetail() {
        DeliveryDetail deliveryDetail = new DeliveryDetail();
        deliveryDetail.setDeliveryStatus(DeliveryStatus.ORDER_RECEIVED);
        deliveryDetail.setCustomerType(CustomerType.VIP);
        deliveryDetail.setExpectedDeliveryTime(LocalTime.now().plusMinutes(25));
        deliveryDetail.setCurrentDistanceFromDestinationInMeters(50);
        deliveryDetail.setTimeToReachDestination(LocalTime.of(00, 10, 10));
        return deliveryDetail;
    }


    @BeforeEach
    void setUp() {
        ticketServiceI = new TicketServiceI(ticketRepository, ticketMapper);
    }

    @Test
    void getAllTickets() {
        ticketServiceI.getAllTickets();
        verify(ticketRepository).findAllByOrderByPriorityAsc();
    }

    @Test
    void createTicket() {
        //when
        ticketServiceI.createTicket(prepareDeliveryDetail(), Priority.HIGH);
        //then
        ArgumentCaptor<Ticket> ticketArgumentCaptor = ArgumentCaptor.forClass(Ticket.class);
        verify(ticketRepository).save(ticketArgumentCaptor.capture());
        Ticket capturedTicket = ticketArgumentCaptor.getValue();
        assertThat(capturedTicket).isNotEqualTo(prepareTicket());
    }

//    @Test
//    void updateTicket() {
//        Ticket ticket = prepareTicket();
//        //when
//        ticketServiceI.updateTicket(ticket,Priority.HIGH);
//        //then
//        ArgumentCaptor<Ticket> ticketArgumentCaptor = ArgumentCaptor.forClass(Ticket.class);
//        verify(ticketRepository).save(ticketArgumentCaptor.capture());
//        Ticket capturedTicket = ticketArgumentCaptor.getValue();
//        assertThat(capturedTicket.getTicketId()).isNotEqualTo(prepareTicket().getTicketId());
//
//    }

    @Test
    void getAlreadyCreatedTicket() {
        DeliveryDetail detail = prepareDeliveryDetail();
        Ticket ticket = prepareTicket();
        given(ticketRepository.findTicketByDeliveryDetail(detail)).willReturn(ticket);
        //when
        ticketServiceI.getAlreadyCreatedTicket(detail);
        //then
        ArgumentCaptor<DeliveryDetail> deliveryDetailArgumentCaptor = ArgumentCaptor.forClass(DeliveryDetail.class);
        verify(ticketRepository).findTicketByDeliveryDetail(deliveryDetailArgumentCaptor.capture());
        DeliveryDetail capturedDeliveryDetail = deliveryDetailArgumentCaptor.getValue();
        assertThat(capturedDeliveryDetail).isEqualTo(detail);
    }
}