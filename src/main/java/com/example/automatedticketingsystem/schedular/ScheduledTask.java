package com.example.automatedticketingsystem.schedular;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.entity.Ticket;
import com.example.automatedticketingsystem.enums.Priority;
import com.example.automatedticketingsystem.properties.AppProperties;
import com.example.automatedticketingsystem.service.CheckDetailToGenerateTicketI;
import com.example.automatedticketingsystem.service.DeliveryDetailServiceI;
import com.example.automatedticketingsystem.service.TicketServiceI;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class ScheduledTask {


    private final DeliveryDetailServiceI deliveryDetailServiceI;
    private final TicketServiceI ticketServiceI;
    private final CheckDetailToGenerateTicketI checkDetailToGenerateTicketI;

    @Autowired
    public ScheduledTask(DeliveryDetailServiceI deliveryDetailServiceI, TicketServiceI ticketServiceI, CheckDetailToGenerateTicketI checkDetailToGenerateTicketI, AppProperties properties) {
        this.deliveryDetailServiceI = deliveryDetailServiceI;
        this.ticketServiceI = ticketServiceI;
        this.checkDetailToGenerateTicketI = checkDetailToGenerateTicketI;
    }


    @Scheduled(cron = "${cron.expression}")
    public void invokeScheduler() {
        log.info("Scheduler start ");
        executeService();
        log.info("Scheduler end");
    }

    private void performAction(DeliveryDetail detail, Priority priority) {
        Ticket ticket = ticketServiceI.getAlreadyCreatedTicket(detail);
        if (ticket != null) {
            ticketServiceI.updateTicket(ticket, priority);
        }
    }

    private void executeService() {
        try {
            List<DeliveryDetail> deliveryDetailList = deliveryDetailServiceI.getAllDeliveryDetails();
            for (DeliveryDetail detail : deliveryDetailList) {
                log.info("Delivery Id : " + detail.getDeliveryId());
                 if (Boolean.TRUE.equals(checkDetailToGenerateTicketI.checkTimeOfDeliveryPassed(detail))) {
                     log.info("Time of Delivery Passed");
                     performAction(detail, Priority.HIGH);
                 }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }


}
