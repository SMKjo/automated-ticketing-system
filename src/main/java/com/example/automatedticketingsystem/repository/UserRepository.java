/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.automatedticketingsystem.repository;

import com.example.automatedticketingsystem.entity.MyUserDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<MyUserDetail, Integer> {


    MyUserDetail findByUsername(String username);

}
