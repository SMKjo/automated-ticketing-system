package com.example.automatedticketingsystem.controller;

import com.example.automatedticketingsystem.enums.CustomerType;
import com.example.automatedticketingsystem.enums.DeliveryStatus;
import com.example.automatedticketingsystem.model.DeliveryDetailDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalTime;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class DeliveryDetailControllerTest {

    @Autowired
    private MockMvc mockMvc;

    public static String asJsonString(Object obj) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void addDeliveryDetails() throws Exception {

        String username = "existUser";
        String password = "password";
        String body = "{\"username\":\"" + username + "\", \"password\":\""
                + password + "\"}";
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/User/Register").header("Content-Type", "application/json").content(body)).andExpect(status().isCreated()).andReturn();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/authenticate").header("Content-Type", "application/json").content(body)).andExpect(status().isOk()).andReturn();
        String response = mvcResult.getResponse().getContentAsString();
        System.out.println("Response :: " + response);
        response = response.replace("{\"jwt\":\"", "");
        String token = response.replace("\"}", "");
        System.out.println("Token : " + token);
        mockMvc.perform(MockMvcRequestBuilders.post("/DeliveryDetails/AddDeliveryDetails")
                        .header("Authorization", "Bearer " + token).header("Content-Type", "application/json").content(asJsonString(prepareDeliveryDetailDto())))
                .andExpect(status().isCreated());

    }

    private DeliveryDetailDto prepareDeliveryDetailDto() {
        DeliveryDetailDto deliveryDetailDto = new DeliveryDetailDto();
        deliveryDetailDto.setDeliveryStatus(DeliveryStatus.ORDER_RECEIVED);
        deliveryDetailDto.setCustomerType(CustomerType.VIP);
        deliveryDetailDto.setExpectedDeliveryTime(LocalTime.now().plusMinutes(25));
        deliveryDetailDto.setCurrentDistanceFromDestinationInMeters(50);
        deliveryDetailDto.setTimeToReachDestination(LocalTime.of(00, 10, 10));
        deliveryDetailDto.setRiderRating(1);
        deliveryDetailDto.setMeanTimeToPrepareFood(LocalTime.of(00, 20, 10));
        return deliveryDetailDto;
    }
}