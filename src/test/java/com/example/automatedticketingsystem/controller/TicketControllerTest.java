package com.example.automatedticketingsystem.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class TicketControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    void getTickets() throws Exception {
        String username = "existUser";
        String password = "password";
        String body = "{\"username\":\"" + username + "\", \"password\":\""
                + password + "\"}";
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/User/Register").header("Content-Type", "application/json").content(body)).andExpect(status().isCreated()).andReturn();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/authenticate").header("Content-Type", "application/json").content(body)).andExpect(status().isOk()).andReturn();
        String response = mvcResult.getResponse().getContentAsString();
        System.out.println("Response :: " + response);
        response = response.replace("{\"jwt\":\"", "");
        String token = response.replace("\"}", "");
        System.out.println("Token : " + token);
        mockMvc.perform(MockMvcRequestBuilders.get("/Ticket/GetTicket")
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk());

    }

}