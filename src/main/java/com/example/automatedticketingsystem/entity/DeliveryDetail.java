package com.example.automatedticketingsystem.entity;

import com.example.automatedticketingsystem.enums.CustomerType;
import com.example.automatedticketingsystem.enums.DeliveryStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalTime;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Delivery_details")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliveryDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "delivery_id")
    private int deliveryId;

    @Enumerated(EnumType.STRING)
    @Column(name = "customer_type", nullable = false)
    private CustomerType customerType;

    @Enumerated(EnumType.STRING)
    @Column(name = "delivery_status", nullable = false)
    private DeliveryStatus deliveryStatus;

    @Column(name = "expected_delivery_time", nullable = false)
    private LocalTime expectedDeliveryTime;

    @Column(name = "current_distance_from_destination_in_meters", nullable = false)
    private int currentDistanceFromDestinationInMeters;

    @Column(name = "time_to_reach_destination", nullable = false)
    private LocalTime timeToReachDestination;

    @OneToOne(mappedBy = "deliveryDetail", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Ticket ticket;


}




