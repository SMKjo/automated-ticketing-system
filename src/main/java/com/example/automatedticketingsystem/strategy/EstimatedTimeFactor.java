package com.example.automatedticketingsystem.strategy;

import com.example.automatedticketingsystem.model.DeliveryDetailDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

@Slf4j
@Component
public class EstimatedTimeFactor implements Factor {

    @Override
    public Boolean applyFactor(DeliveryDetailDto detail) {
        if (checkEstimatedTimePassed(detail)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public FactorName getFactorName() {
        return FactorName.IsEstimatedTimeAfter;
    }

    private Boolean checkEstimatedTimePassed(DeliveryDetailDto detail) {
        LocalTime totalTime = detail.getMeanTimeToPrepareFood().plusHours(detail.getTimeToReachDestination().getHour()).plusMinutes(detail.getTimeToReachDestination().getMinute()).plusSeconds(detail.getTimeToReachDestination().getSecond());
        LocalTime estimatedTime = LocalTime.now().plusHours(totalTime.getHour()).plusMinutes(totalTime.getMinute()).plusSeconds(totalTime.getSecond());
        log.info("Estimated Time " + estimatedTime + " Is After " + "Expected Time " + detail.getExpectedDeliveryTime());
        if (estimatedTime.isAfter(detail.getExpectedDeliveryTime())) {
            log.info("TRUE");
            return true;
        } else {
            log.info("FALSE");
            return false;
        }
    }
}
