package com.example.automatedticketingsystem.service;

import com.example.automatedticketingsystem.entity.MyUserDetail;
import com.example.automatedticketingsystem.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MyUserDetailServiceITest {

    @Mock
    private UserRepository userRepository;
    private MyUserDetailServiceI myUserDetailService;

    @BeforeEach
    void setUp() {
        myUserDetailService = new MyUserDetailServiceI(userRepository);
    }

    @Test
    void canLoadUserByUsername() {
        //given
        String username = "username";
        MyUserDetail userDetail = new MyUserDetail(1, "username", "abcd@1234");
        given(userRepository.findByUsername(username)).willReturn(userDetail);
        //when
        myUserDetailService.loadUserByUsername("username");
        //then
        ArgumentCaptor<String> userNameArgCaptor = ArgumentCaptor.forClass(String.class);
        verify(userRepository).findByUsername(userNameArgCaptor.capture());
        String capturedUserName = userNameArgCaptor.getValue();
        assertThat(capturedUserName).isEqualTo("username");
    }

    @Test
    void addUserDetails() {
        MyUserDetail userDetail = new MyUserDetail(1, "username", "abcd@1234");
        // when
        myUserDetailService.AddUserDetails(userDetail);
        // then
        ArgumentCaptor<MyUserDetail> userArgumentCaptor = ArgumentCaptor.forClass(MyUserDetail.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        MyUserDetail capturedUser = userArgumentCaptor.getValue();
        assertThat(capturedUser).isEqualTo(userDetail);
    }
}