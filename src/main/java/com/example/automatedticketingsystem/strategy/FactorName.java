package com.example.automatedticketingsystem.strategy;

public enum FactorName {
    IsVIP,
    IsExpectedTimePassed,
    IsEstimatedTimeAfter
}
