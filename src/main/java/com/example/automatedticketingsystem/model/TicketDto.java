package com.example.automatedticketingsystem.model;

import com.example.automatedticketingsystem.enums.CustomerType;
import com.example.automatedticketingsystem.enums.DeliveryStatus;
import com.example.automatedticketingsystem.enums.Priority;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Getter
@Setter
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TicketDto {

    private int ticketId;
    private Priority priority;
    private CustomerType customerType;
    private DeliveryStatus deliveryStatus;
    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime expectedDeliveryTime;
    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime timeToReachDestination;
    private String riderRating;
    private int distanceFromDestination;

}
