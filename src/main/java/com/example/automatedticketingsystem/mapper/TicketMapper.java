package com.example.automatedticketingsystem.mapper;

import com.example.automatedticketingsystem.entity.Ticket;
import com.example.automatedticketingsystem.model.TicketDto;
import org.springframework.stereotype.Component;

@Component
public class TicketMapper {

    public TicketDto TicketEntityToDtoMapper(Ticket ticket) {
        TicketDto ticketDto = new TicketDto();
        ticketDto.setPriority(ticket.getPriority());
        ticketDto.setCustomerType(ticket.getDeliveryDetail().getCustomerType());
        ticketDto.setExpectedDeliveryTime(ticket.getDeliveryDetail().getExpectedDeliveryTime());
        ticketDto.setDeliveryStatus(ticket.getDeliveryDetail().getDeliveryStatus());
        ticketDto.setDistanceFromDestination(ticket.getDeliveryDetail().getCurrentDistanceFromDestinationInMeters());
        ticketDto.setTimeToReachDestination(ticket.getDeliveryDetail().getTimeToReachDestination());
        return ticketDto;
    }
}
