package com.example.automatedticketingsystem.controller;

import com.example.automatedticketingsystem.entity.MyUserDetail;
import com.example.automatedticketingsystem.service.MyUserDetailServiceI;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/User")
public class MyUserDetailController {


    private final MyUserDetailServiceI myUserDetailServiceI;

    public MyUserDetailController(MyUserDetailServiceI myUserDetailServiceI) {
        this.myUserDetailServiceI = myUserDetailServiceI;
    }


    @PostMapping(value = "/Register")
    public ResponseEntity<?> createUserDetails(@Valid @RequestBody MyUserDetail details) {
        return myUserDetailServiceI.AddUserDetails(details);
    }

}
