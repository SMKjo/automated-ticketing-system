package com.example.automatedticketingsystem.strategy;

import com.example.automatedticketingsystem.enums.CustomerType;
import com.example.automatedticketingsystem.model.DeliveryDetailDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class VipCustomerFactor implements Factor {


    @Override
    public Boolean applyFactor(DeliveryDetailDto detail) {
        if (checkIsVip(detail)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public FactorName getFactorName() {
        return FactorName.IsVIP;
    }

    private Boolean checkIsVip(DeliveryDetailDto deliveryDetailDto) {
        log.info("Is Customer type is VIP");
        if (deliveryDetailDto.getCustomerType().equals(CustomerType.VIP)) {
            log.info("TRUE");
            return true;
        } else {
            log.info("FALSE");
            return false;
        }
    }
}
