package com.example.automatedticketingsystem.service;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.enums.CustomerType;
import com.example.automatedticketingsystem.enums.DeliveryStatus;
import com.example.automatedticketingsystem.model.DeliveryDetailDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalTime;

@Slf4j
@Service
public class CheckDetailToGenerateTicketI implements CheckDetailToGenerateTicket {


    @Override
    public Boolean checkTimeOfDeliveryPassed(DeliveryDetail detail) {
        log.info("Expected Delivery time " + detail.getExpectedDeliveryTime() + " is before " + LocalTime.now() + " and delivery status " + detail.getDeliveryStatus());
        if (detail.getExpectedDeliveryTime().isBefore(LocalTime.now()) && !detail.getDeliveryStatus().equals(DeliveryStatus.ORDER_DELIVERED)) {
            log.info(String.valueOf(detail.getExpectedDeliveryTime().isBefore(LocalTime.now()) && !detail.getDeliveryStatus().equals(DeliveryStatus.ORDER_DELIVERED)));
            log.info("TRUE");
            return true;
        } else {
            log.info("FALSE");
            return false;
        }
    }

    @Override
    public Boolean checkEstimatedTimeOfDelivery(DeliveryDetailDto detail) {
        LocalTime totalTime = detail.getMeanTimeToPrepareFood().plusHours(detail.getTimeToReachDestination().getHour()).plusMinutes(detail.getTimeToReachDestination().getMinute()).plusSeconds(detail.getTimeToReachDestination().getSecond());
        LocalTime estimatedTime = LocalTime.now().plusHours(totalTime.getHour()).plusMinutes(totalTime.getMinute()).plusSeconds(totalTime.getSecond());
        log.info("Estimated Time " + estimatedTime + " Is After " + "Expected Time " + detail.getExpectedDeliveryTime());
        if (estimatedTime.isAfter(detail.getExpectedDeliveryTime())) {
            log.info("TRUE");
            return true;
        } else {
            log.info("FALSE");
            return false;
        }
    }

    @Override
    public Boolean checkCustomerType(DeliveryDetail detail) {
        log.info("Is Customer type VIP");
        if (detail.getCustomerType().equals(CustomerType.VIP)) {
            log.info("TRUE");
            return true;
        } else {
            log.info("FALSE");
            return false;
        }
    }
}
