package com.example.automatedticketingsystem.mapper;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.model.DeliveryDetailDto;
import org.springframework.stereotype.Component;

@Component
public class DeliveryDetailMapper {

    public DeliveryDetail DeliveryDetailDtoToEntityMapper(DeliveryDetailDto dto) {
        DeliveryDetail deliveryDetail = new DeliveryDetail();
        deliveryDetail.setDeliveryStatus(dto.getDeliveryStatus());
        deliveryDetail.setCustomerType(dto.getCustomerType());
        deliveryDetail.setExpectedDeliveryTime(dto.getExpectedDeliveryTime());
        deliveryDetail.setCurrentDistanceFromDestinationInMeters(dto.getCurrentDistanceFromDestinationInMeters());
        deliveryDetail.setTimeToReachDestination(dto.getTimeToReachDestination());
        return deliveryDetail;
    }
}
