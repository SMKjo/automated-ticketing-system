package com.example.automatedticketingsystem.strategy;

import com.example.automatedticketingsystem.model.DeliveryDetailDto;

public interface Factor {
    Boolean applyFactor(DeliveryDetailDto detail);

    FactorName getFactorName();
}
