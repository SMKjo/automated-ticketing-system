package com.example.automatedticketingsystem.enums;

public enum Priority {
    HIGH, MEDIUM, LOW;
}
