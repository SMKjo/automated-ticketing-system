package com.example.automatedticketingsystem.service;

import com.example.automatedticketingsystem.entity.MyUserDetail;
import com.example.automatedticketingsystem.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Service
public class MyUserDetailServiceI implements MyUserDetailService {


    private final UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    public MyUserDetailServiceI(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        MyUserDetail userDetail;
        try {
            userDetail = userRepository.findByUsername(userName);
        } catch (UsernameNotFoundException e) {
            throw new UsernameNotFoundException("Username not found " + e);
        }
        return new User(userDetail.getUsername(), userDetail.getPassword(), new ArrayList<>());
    }

    public ResponseEntity<?> AddUserDetails(MyUserDetail userDetail) {
        try {
            userDetail.setPassword(passwordEncoder.encode(userDetail.getPassword()));
            MyUserDetail detail = userRepository.save(userDetail);
            log.info("User details added successfully");
            return new ResponseEntity<>(detail, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Unable to add user details", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
