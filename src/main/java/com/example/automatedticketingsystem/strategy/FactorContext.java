package com.example.automatedticketingsystem.strategy;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Slf4j
@Component
@AllArgsConstructor
public class FactorContext {

    private Map<FactorName, Factor> factors;

    @Autowired
    public FactorContext(Set<Factor> factorSet) {
        createFactor(factorSet);
    }

    private void createFactor(Set<Factor> factorSet) {

        factors = new HashMap<>();
        factorSet.forEach(factor -> factors.put(factor.getFactorName(), factor));
    }

    public Factor findFactor(FactorName factorName) {
        return factors.get(factorName);
    }


}
