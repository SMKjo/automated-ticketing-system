package com.example.automatedticketingsystem.controller;

import com.example.automatedticketingsystem.model.DeliveryDetailDto;
import com.example.automatedticketingsystem.service.DeliveryDetailServiceI;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/DeliveryDetails")
public class DeliveryDetailController {


    private final DeliveryDetailServiceI deliveryDetailServiceI;

    public DeliveryDetailController(DeliveryDetailServiceI deliveryDetailServiceI) {
        this.deliveryDetailServiceI = deliveryDetailServiceI;
    }

    @PostMapping(value = "/AddDeliveryDetails")
    public ResponseEntity<?> AddDeliveryDetails(@Valid @RequestBody DeliveryDetailDto deliveryDetails) {
        return deliveryDetailServiceI.AddDeliveryDetails(deliveryDetails);
    }

    @PostMapping(value = "/AddDeliveryDetailsStrategy")
    public ResponseEntity<?> AddDeliveryDetailsStrategy(@Valid @RequestBody DeliveryDetailDto deliveryDetails) {
        return deliveryDetailServiceI.AddDeliveryDetailsStrategy(deliveryDetails);
    }


}
