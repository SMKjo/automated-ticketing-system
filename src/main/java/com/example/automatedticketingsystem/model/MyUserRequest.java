package com.example.automatedticketingsystem.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MyUserRequest {

    private String username;
    private String password;
}
