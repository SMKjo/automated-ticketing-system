package com.example.automatedticketingsystem.service;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.model.DeliveryDetailDto;

public interface CheckDetailToGenerateTicket {
    Boolean checkTimeOfDeliveryPassed(DeliveryDetail detail);

    Boolean checkCustomerType(DeliveryDetail detail);

    Boolean checkEstimatedTimeOfDelivery(DeliveryDetailDto detailDto);

}
