package com.example.automatedticketingsystem.model;

import com.example.automatedticketingsystem.enums.CustomerType;
import com.example.automatedticketingsystem.enums.DeliveryStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.time.LocalTime;

@Getter
@Setter
public class DeliveryDetailDto {

    @NotEmpty
    private int deliveryId;

    @NotEmpty
    private CustomerType customerType;

    @NotEmpty
    private DeliveryStatus deliveryStatus;

    @NotEmpty
    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime expectedDeliveryTime;

    @NotEmpty
    private int currentDistanceFromDestinationInMeters;

    @NotEmpty
    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime timeToReachDestination;

    @NotEmpty
    private int riderRating;

    @NotEmpty
    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime meanTimeToPrepareFood;

}




