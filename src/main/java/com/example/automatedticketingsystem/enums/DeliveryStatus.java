package com.example.automatedticketingsystem.enums;

public enum DeliveryStatus {
    ORDER_RECEIVED, ORDER_PREPARING, ORDER_PICKEDUP, ORDER_DELIVERED;
}
