package com.example.automatedticketingsystem.service;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.model.DeliveryDetailDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface DeliveryDetailService {
    ResponseEntity<?> AddDeliveryDetails(DeliveryDetailDto deliveryDetail);

    ResponseEntity<?> AddDeliveryDetailsStrategy(DeliveryDetailDto deliveryDetail);

    List<DeliveryDetail> getAllDeliveryDetails();

}
