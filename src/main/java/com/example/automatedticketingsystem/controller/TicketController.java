package com.example.automatedticketingsystem.controller;

import com.example.automatedticketingsystem.model.TicketDto;
import com.example.automatedticketingsystem.service.TicketServiceI;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/Ticket")
public class TicketController {


    private final TicketServiceI ticketServiceI;

    public TicketController(TicketServiceI ticketServiceI) {
        this.ticketServiceI = ticketServiceI;
    }


    @GetMapping(value = "/GetTicket")
    public ResponseEntity<?> getTickets() {
        List<TicketDto> ticketList;
        try {
            ticketList = ticketServiceI.getAllTickets();
            return new ResponseEntity<>(ticketList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

}
