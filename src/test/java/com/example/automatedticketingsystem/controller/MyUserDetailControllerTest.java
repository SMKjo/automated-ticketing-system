package com.example.automatedticketingsystem.controller;

import com.example.automatedticketingsystem.model.MyUserRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
class MyUserDetailControllerTest {

    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void createUserDetailsTest() {
        MyUserRequest myUserRequest = new MyUserRequest("username", "password");
        ResponseEntity responseEntity = testRestTemplate.postForEntity(createURLWithPort("/User/Register"), myUserRequest, String.class);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}