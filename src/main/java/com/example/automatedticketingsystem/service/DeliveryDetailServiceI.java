package com.example.automatedticketingsystem.service;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.enums.Priority;
import com.example.automatedticketingsystem.mapper.DeliveryDetailMapper;
import com.example.automatedticketingsystem.model.DeliveryDetailDto;
import com.example.automatedticketingsystem.repository.DeliveryDetailRepository;
import com.example.automatedticketingsystem.strategy.FactorContext;
import com.example.automatedticketingsystem.strategy.FactorName;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class DeliveryDetailServiceI implements DeliveryDetailService {

    private final DeliveryDetailRepository deliveryDetailRepository;
    private final DeliveryDetailMapper deliveryDetailMapper;
    private final TicketServiceI ticketServiceI;
    private final CheckDetailToGenerateTicketI checkDetailToGenerateTicketI;
    @Autowired
    private FactorContext factorContext;

    @Autowired
    public DeliveryDetailServiceI(DeliveryDetailRepository deliveryDetailRepository, DeliveryDetailMapper deliveryDetailMapper, TicketServiceI ticketServiceI, CheckDetailToGenerateTicketI checkDetailToGenerateTicketI) {
        this.deliveryDetailRepository = deliveryDetailRepository;
        this.deliveryDetailMapper = deliveryDetailMapper;
        this.ticketServiceI = ticketServiceI;
        this.checkDetailToGenerateTicketI = checkDetailToGenerateTicketI;
    }

    @Override
    public ResponseEntity<?> AddDeliveryDetails(DeliveryDetailDto deliveryDetail) {
        try {
            DeliveryDetail detail = deliveryDetailRepository.save(deliveryDetailMapper.DeliveryDetailDtoToEntityMapper(deliveryDetail));
            log.info("Delivery details added successfully");
            if (Boolean.TRUE.equals(checkDetailToGenerateTicketI.checkTimeOfDeliveryPassed(detail))) {
                ticketServiceI.createTicket(detail, Priority.HIGH);
            } else if (Boolean.TRUE.equals(checkDetailToGenerateTicketI.checkCustomerType(detail))) {
                ticketServiceI.createTicket(detail, Priority.HIGH);
            } else if (Boolean.TRUE.equals(checkDetailToGenerateTicketI.checkEstimatedTimeOfDelivery(deliveryDetail))) {
                ticketServiceI.createTicket(detail, Priority.MEDIUM);
            } else {
                ticketServiceI.createTicket(detail, Priority.LOW);
            }
            return new ResponseEntity<>(deliveryDetail, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Unable to add delivery details", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public List<DeliveryDetail> getAllDeliveryDetails() {
        log.info("Inside get All Delivery Details");
        return deliveryDetailRepository.findAll();
    }

    @Override
    public ResponseEntity<?> AddDeliveryDetailsStrategy(DeliveryDetailDto deliveryDetail) {
        try {
            generateTicketBasedOnStrategies(deliveryDetail);
            return new ResponseEntity<>(deliveryDetail, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Unable to add delivery details", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private void generateTicketBasedOnStrategies(DeliveryDetailDto deliveryDetail) throws Exception {
        try {
            if (Boolean.TRUE.equals(factorContext.findFactor(FactorName.IsVIP).applyFactor(deliveryDetail))) {
                ticketServiceI.createTicket(deliveryDetailMapper.DeliveryDetailDtoToEntityMapper(deliveryDetail), Priority.HIGH);
            } else if (Boolean.TRUE.equals(factorContext.findFactor(FactorName.IsExpectedTimePassed).applyFactor(deliveryDetail))) {
                ticketServiceI.createTicket(deliveryDetailMapper.DeliveryDetailDtoToEntityMapper(deliveryDetail), Priority.HIGH);
            } else if (Boolean.TRUE.equals(factorContext.findFactor(FactorName.IsEstimatedTimeAfter).applyFactor(deliveryDetail))) {
                ticketServiceI.createTicket(deliveryDetailMapper.DeliveryDetailDtoToEntityMapper(deliveryDetail), Priority.MEDIUM);
            } else {
                ticketServiceI.createTicket(deliveryDetailMapper.DeliveryDetailDtoToEntityMapper(deliveryDetail), Priority.LOW);
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }


}
