package com.example.automatedticketingsystem.service;

import com.example.automatedticketingsystem.entity.DeliveryDetail;
import com.example.automatedticketingsystem.entity.Ticket;
import com.example.automatedticketingsystem.enums.Priority;
import com.example.automatedticketingsystem.model.TicketDto;

import java.util.List;

public interface TicketService {
    List<TicketDto> getAllTickets();

    void createTicket(DeliveryDetail deliveryDetail, Priority priority);

    void updateTicket(Ticket ticket, Priority priority);

    Ticket getAlreadyCreatedTicket(DeliveryDetail detail);
}
